﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public static GameControl instance;
    public GameObject gameOverText;
    public Text scoreText;
    public bool gameOver = false;
    public float scrollSpeed = -1.5f;

    public int HighScore { get; protected set; } // {...} permet de protéger le set (contenu) seulement modifiable par "nous"   

    private int score = 0;
        public int Score
    {
        get
        {
            return score; 
        }

        protected set
        {
            // Si la nouvelle valeur est différente de l'ancienne valeur 
                score = value;
                if (score > HighScore)
                {
                    HighScore = score;
                    PlayerPrefs.SetInt("HighScore", HighScore); // à éviter, pas du tout sécurisé, facilement hackable et modifiable 
                }
        }
    }
   
	// Use this for initialization
	void Awake ()
    {
		if (instance == null) {
            instance = this;
        } else if (instance != this)
        {
            Destroy (gameObject);
        }

        HighScore = PlayerPrefs.GetInt("HighScore", 0);

	}
	
	// Update is called once per frame
	void Update ()
    {
		if (gameOver == true && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}

    public void BirdScored()
    {
        if (gameOver)
        {
            return;
        }
        Score++;
        scoreText.text = "Score: " + Score.ToString ();
    }
   


    public void BirdDied()
    {
        gameOverText.SetActive (true);
        gameOver = true;
    }


}
